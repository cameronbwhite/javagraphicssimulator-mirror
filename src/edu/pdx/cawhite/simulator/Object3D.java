/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.simulator;

import java.awt.Graphics;

import edu.pdx.cawhite.iterators.IteratorException;
import edu.pdx.cawhite.math.ColumnMatrix;
import edu.pdx.cawhite.math.LengthException;
import edu.pdx.cawhite.math.MatrixSizeException;
import edu.pdx.cawhite.math.coordinates.Coordinate;
import edu.pdx.cawhite.math.coordinates.CoordinateMatrix;
import edu.pdx.cawhite.math.coordinates.Functions2D;
import edu.pdx.cawhite.math.coordinates.Functions3D;
import edu.pdx.cawhite.math.transformations.Projection3D;
import edu.pdx.cawhite.math.transformations.Rotation3D;
import edu.pdx.cawhite.math.transformations.Translation3D;

/**
 * @author Cameron Brandon White
 */
public class Object3D extends Object<Enviroment3D> {

    protected Coordinate leftPoint, rightPoint,
                     topPoint, bottomPoint,
                     frontPoint, backPoint;

    /**
     * Create a new 3D object.
     *
     * @param enviroment    The environment to add the object to.
     * @param items         The items.
     */
    public Object3D(Enviroment3D enviroment, Coordinate center, double size) throws LengthException {
        this(center, size);
        this.enviroment = enviroment;
        this.enviroment.addObject(this);
    }

    public Object3D(Enviroment3D enviromont, double[][] components) {
        super(components);
        this.enviroment = enviroment;
        this.enviroment.addObject(this);
    }

    public Object3D(Enviroment3D enviromont, CoordinateMatrix matrix) {
        super(matrix.getComponents());
        this.enviroment = enviroment;
        this.enviroment.addObject(this);
    }

    protected Object3D(Coordinate center, double size) throws LengthException {
        super(Functions3D.cube(center, size, 1.0));
        this.center = center;

        try {
            rightPoint  = new Coordinate(new double[] {
                center.get(0)+(size/2), center.get(1), center.get(2)});
            leftPoint   = new Coordinate(new double[] {
                center.get(0)-(size/2), center.get(1), center.get(2)});
            topPoint    = new Coordinate(new double[] {
                center.get(0), center.get(1)+(size/2), center.get(2)});
            bottomPoint = new Coordinate(new double[] {
                center.get(0), center.get(1)-(size/2), center.get(2)});
            frontPoint  = new Coordinate(new double[] {
                center.get(0), center.get(1), center.get(2)+(size/2)});
            backPoint   = new Coordinate(new double[] {
                center.get(0), center.get(1), center.get(2)-(size/2)});
        } catch (LengthException e) {
            
        }
    }

    public synchronized void paint(Graphics g, Player3D player) throws LengthException, MatrixSizeException {
    	
    	Projection3D projection = new Projection3D(10.0);
    	
        CoordinateMatrix projectedCoordinateMatrix = projection.transform(this);
        Iterator iterator = projectedCoordinateMatrix.getIterator();

        while (true) { 
            try {
                Coordinate projectedCoordinate = iterator.get();
                //System.out.print(projectedCoordinate.get(0));
                //System.out.print(" ");
                //System.out.println(projectedCoordinate.get(1));
                g.drawRect((int) Math.round(projectedCoordinate.get(0)),
                           (int) Math.round(projectedCoordinate.get(1)), 1, 1);
                iterator.next();
            } catch (IteratorException e) { 
                 break;
            }
        }
    }

    public void moveAlongLine(Coordinate point1, Coordinate point2) 
            throws LengthException {
        moveAlongLine(point1, point2, 1.0);
    }

    public void moveAlongLine(Coordinate point1, Coordinate point2, double distance) 
            throws LengthException {
        Coordinate distanceVector = null;
        distanceVector = point2.sub(point1);
        distanceVector.inormalize();
        if (distance != 1.0)
            distanceVector.imul(distance);
        double x = distanceVector.get(0);
        double y = distanceVector.get(1);
        double z = distanceVector.get(2);
        translate(x, y, z);
    }

    public void moveDown() {
        moveDown(1.0);
    }

    public void moveDown(double distance) {
        try {
            moveAlongLine(topPoint, bottomPoint, distance);
        } catch (LengthException e) {
            assert false : "Fatal programming error";
        }
    }

    public void moveUp() {
        moveUp(1.0);
    }

    public void moveUp(double distance) {
        try {
            moveAlongLine(bottomPoint, topPoint, distance);
        } catch (LengthException e) {
            assert false : "Fatal programming error";
        }
    }

    public void moveLeft() {
        moveLeft(1.0);
    }

    public void moveLeft(double distance) {
        try {
            moveAlongLine(rightPoint, leftPoint, distance);
        } catch (LengthException e) {
            assert false : "Fatal programming error";
        }
    }

    public void moveRight() {
        moveRight(1.0);
    }

    public void moveRight(double distance) {
        try {
            moveAlongLine(leftPoint, rightPoint, distance);
        } catch (LengthException e) {
            assert false : "Fatal programming error";
        }
    }

    public void moveForward() {
        moveForward(1.0);
    }

    public void moveForward(double distance) {
        try {
            moveAlongLine(backPoint, frontPoint, distance);
        } catch (LengthException e) {
            assert false : "Fatal programming error";
        }
    }

    public void moveBack() {
        moveBack(1.0);
    }

    public void moveBack(double distance) {
        try {
            moveAlongLine(frontPoint, backPoint, distance);
        } catch (LengthException e) {
            assert false : "Fatal programming error";
        }
    }

    public void pitchMovement(double t) {
        this.rotate(this.leftPoint, this.rightPoint, t);
    }

    public void yawMovement(double t) {
        this.rotate(this.bottomPoint, this.topPoint, t);
    }

    public void rollMovement(double t) {
        this.rotate(this.backPoint, this.frontPoint, t);
    }


    protected void rotate(Coordinate point1, Coordinate point2, double t) {
    	Rotation3D rotation = new Rotation3D(point1, point2, t);
    	try {
    		this.set(rotation.transform(this));
    		this.center = rotation.transform(this.center);
            this.leftPoint = rotation.transform(this.leftPoint);
            this.rightPoint = rotation.transform(this.rightPoint);
            this.topPoint = rotation.transform(this.topPoint);
            this.bottomPoint = rotation.transform(this.bottomPoint);
            this.backPoint = rotation.transform(this.backPoint);
            this.frontPoint = rotation.transform(this.frontPoint);
    	} catch (MatrixSizeException e) {
    	} catch (LengthException e) {
    		
    	}
    }

    protected void translate(double x, double y, double z) {
        Translation3D translation = new Translation3D(x, y, z);
        try {
            this.set(translation.transform(this));
            this.center = translation.transform(this.center);
            this.leftPoint = translation.transform(this.leftPoint);
            this.rightPoint = translation.transform(this.rightPoint);
            this.topPoint = translation.transform(this.topPoint);
            this.bottomPoint = translation.transform(this.bottomPoint);
            this.frontPoint = translation.transform(this.frontPoint);
            this.backPoint = translation.transform(this.backPoint);
        } catch (MatrixSizeException e) {
            assert false;
        } catch (LengthException e) {
            assert false;
        }        
    }
}