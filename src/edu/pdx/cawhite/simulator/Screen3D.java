/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.simulator;

import java.awt.Graphics;

import edu.pdx.cawhite.containers.DList;
import edu.pdx.cawhite.math.LengthException;
import edu.pdx.cawhite.math.MatrixSizeException;
import edu.pdx.cawhite.simulator.Enviroment3D;

public class Screen3D extends Screen<Player3D, Object3D, Enviroment3D> implements Runnable {

    public boolean isYawRightNeeded;
	private boolean isYawLeftNeeded;
	private boolean isPitchUpNeeded;
	private boolean isPitchDownNeeded;
	private boolean isRollRightNeeded;
	private boolean isRollLeftNeeded;
	public boolean isRollUpNeeded;
	public boolean isRollDownNeeded;


	/**
     *
     */
    public Screen3D(Enviroment3D enviroment, Player3D player,
                    int width, int length) {

        super(enviroment, player, width, length);
    }

    /**
     *
     */
    public void run() {

        Player3D player = getPlayer();

        while(true) {
            if (isUpPressed) {
                player.moveDown();
                isRepaintNeeded = true;
            }
            if (isDownPressed) {
                player.moveUp();
                isRepaintNeeded = true;
            }
            if (isLeftPressed) {
                player.moveLeft();
                isRepaintNeeded = true;
            }
            if (isRightPressed) {
                player.moveRight();
                isRepaintNeeded = true;
            }
            if (isTurnLeftPressed) {
                player.moveBack();
                isRepaintNeeded = true;
            }
            if (isTurnRightPressed) {
                player.moveForward();
                isRepaintNeeded = true;
            }
            if (isYawRightNeeded) {
                player.yawMovement(1.0);
                isRepaintNeeded = true;
            }
            if (isYawLeftNeeded) {
                player.yawMovement(-1.0);
                isRepaintNeeded = true;
            }
            if (isPitchUpNeeded) {
                player.pitchMovement(1.0);
                isRepaintNeeded = true;
            }
            if (isPitchDownNeeded) {
                player.pitchMovement(-1.0);
                isRepaintNeeded = true;
            }
            if (isRollRightNeeded) {
                player.rollMovement(1.0);
                isRepaintNeeded = true;
            }
            if (isRollLeftNeeded) {
                player.rollMovement(-1.0);
                isRepaintNeeded = true;
            }
            if (isRepaintNeeded) {
                repaint();
                isRepaintNeeded = false;
            }
        }
    }

        /**
     *
     */
    public void paint(Graphics g) {
    	try {
    		player.paint(g, player);
    		paintObjects(g);
    	} catch (LengthException e) {
    		
    	} catch (MatrixSizeException e) {
 
    	}
    }

    /**
     *
     */
    protected void paintObjects(Graphics g) {
        Object3D object;
        DList<Object3D> list = enviroment.getObjects();
        DList<Object3D>.Iterator listIterator = list.new Iterator();

    }
}
