/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.math.transformations;

import edu.pdx.cawhite.math.LengthException;
import edu.pdx.cawhite.math.coordinates.Coordinate;

/**
 * Implement a 3D rotation transformation.
 * 
 * @author Cameron Brandon White
 */
public class Rotation3D extends Transformation {

    /**
     * Create a rotation about the line.
     * 
     * @param point1 The first point of the line
     * @param point2 The second point of the line
     * @param t The amount to rotate
     */
    public Rotation3D(Coordinate point1, Coordinate point2, double t) {
    	Coordinate vector = null;
    	try {
    		vector = point2.sub(point1);
    	} catch (LengthException e) {
    	}
        vector.inormalize();

        double a = point1.get(0);
        double b = point1.get(1);
        double c = point1.get(2);
        double u = vector.get(0);
        double v = vector.get(1);
        double w = vector.get(2);

        double uu = u*u;
        double vv = v*v;
        double ww = w*w;

        double cosT = Math.cos(t);
        double oneMinusCosT = 1 - cosT;
        double sinT = Math.sin(t);

        double m11 = uu + (vv + ww) * cosT;
        double m12 = u*v * oneMinusCosT - w*sinT;
        double m13 = u*w * oneMinusCosT + v*sinT;
        double m14 = (a*(vv + ww) - u*(b*v + c*w))*oneMinusCosT
            + (b*w - c*v)*sinT;

        double m21 = u*v * oneMinusCosT + w*sinT;
        double m22 = vv + (uu + ww) * cosT;
        double m23 = v*w * oneMinusCosT - u*sinT;
        double m24 = (b*(uu + ww) - v*(a*u + c*w))*oneMinusCosT
            + (c*u - a*w)*sinT;

        double m31 = u*w * oneMinusCosT - v*sinT;
        double m32 = v*w * oneMinusCosT + u*sinT;
        double m33 = ww + (uu + vv) * cosT;
        double m34 = (c*(uu + vv) - w*(a*u + b*v))*oneMinusCosT
            + (a*v - b*u)*sinT;

        this.matrix = new TransformationMatrix(new double[][] {
            {m11, m12, m13, m14},
            {m21, m22, m23, m24},
            {m31, m32, m33, m34},
            {0.0, 0.0, 0.0, 1.0}});
    }
}
