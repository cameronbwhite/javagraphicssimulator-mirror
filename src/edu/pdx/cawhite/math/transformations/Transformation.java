/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.math.transformations;

import edu.pdx.cawhite.math.LengthException;
import edu.pdx.cawhite.math.MatrixSizeException;
import edu.pdx.cawhite.math.coordinates.Coordinate;
import edu.pdx.cawhite.math.coordinates.CoordinateMatrix;

/**
 * Abstract transformation
 *
 * @author Cameron Brandon White
 */
public abstract class Transformation {
	
	protected TransformationMatrix matrix;

    /**
     * Transform a coordinate
     *
     * @param corrdinate The coordinate to tranform
     * @return The new transformed coordinate
     */    
	public Coordinate transform(Coordinate coordinate) throws LengthException {
		return this.matrix.mul(coordinate);
	}

    /**
     * Transform a coordinate into the Coordinate supplied
     *
     * @param coordinate The coordinate to transform
     * @param newCoordinate The coordinate to place the transformed coordinate
     */
	public Coordinate transform(Coordinate coordinate, 
							    Coordinate newCoordinate) 
								throws LengthException {
		return this.matrix.mul(coordinate, newCoordinate);
	}

    /**
     * Transform a coordinateMatrix
     *
     * @param coordinateMatrix The matrix to transform
     */
	public CoordinateMatrix transform(CoordinateMatrix coordinateMatrix) 
								      throws MatrixSizeException {
		return this.matrix.mul(coordinateMatrix);
	}

    /**
     * Transform a coordinateMatrix into the Matrix supplied
     *
     * @param coordinateMatrix The matrix to transform
     * @param newCoordinatematrix The matrix to place the transformed matrix
     * @return The transformed matrix
     */
	public CoordinateMatrix transform(CoordinateMatrix coordinateMatrix, 
									  CoordinateMatrix newCoordinatematrix) 
									  throws MatrixSizeException {
		return this.matrix.mul(coordinateMatrix, newCoordinatematrix);
	}
}
