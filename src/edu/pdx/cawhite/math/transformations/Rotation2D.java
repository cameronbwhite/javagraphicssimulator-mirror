/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.math.transformations;

import edu.pdx.cawhite.math.coordinates.Coordinate;

/**
 * Implements a 2D Rotation transformation
 *
 * @author Cameron Brandon White
 */
public class Rotation2D extends Transformation {

	/**
	 * Create a standard 2D rotation matrix about the origin.
	 *
	 * @param t The amount to rotate
	 */
	public Rotation2D(double t) {

		this.matrix = new TransformationMatrix(new double[][] {
	        {Math.cos(t),  -Math.sin(t),  0.0},
	        {Math.sin(t),   Math.cos(t),  0.0},
	        {0.0,           0.0,          1.0}});
	}

	/**
	 * Create a 2D rotation matrix about the point.
	 *
	 * @param point The point to rotate about
	 * @param t The amount to rotate
	 */
	public Rotation2D(Coordinate point, double t) {
		double x = point.get(0);
		double y = point.get(1);
		this.matrix = new TransformationMatrix(new double[][] {
            {Math.cos(t),  -Math.sin(t),  x - x*Math.cos(t) + y*Math.sin(t)},
            {Math.sin(t),   Math.cos(t),  y - x*Math.sin(t) - y*Math.cos(t)},
            {        0.0,           0.0,                                1.0}});
	}
}
