/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.math.coordinates;

import edu.pdx.cawhite.math.Vector;
import edu.pdx.cawhite.math.LengthException;

/**
 * Coordinate is just like a Vector but the last element 
 * is always 1.0.
 * 
 * @author Cameron Brandon White
 */
public class Coordinate extends Vector {

	public Coordinate(int length) throws LengthException {
		super();
		if (length < 1)
			throw new LengthException("Length must be greater than 1.");
		this.components = new double[length - 1];
	}

	public Coordinate(double[] components) throws LengthException {
		super();
    	if (components.length <= 0)
    		throw new LengthException("Length must be greater than 0.");
		this.components = components;
	}

	public Coordinate(Vector vector) throws LengthException {
		super();
    	this.components = new double[vector.getLength()];
    	this.copy(vector);
	}

	public Coordinate(Coordinate coordinate) throws LengthException {
		super();
    	this.components = new double[coordinate.getLength()];
    	this.copy(coordinate);
	}

    /** 
     * Add this matrix with the other in a new vector.
     * 
     * @param other The other vector.
     * @return The new vector. 
     */
	public Coordinate add(Coordinate other) throws LengthException {

		Coordinate newCoordinate = null;

		try {
			newCoordinate = new Coordinate(getLength());
		} catch (LengthException e) {
			assert false : "Fatal programming error";
		}

		return add(other, newCoordinate);
	}

    /** 
     * Add this matrix with the other in a new vector.
     * 
     * @param other The other vector.
     * @return The new vector. 
     */
	public Coordinate add(Vector other) throws LengthException {

		Coordinate newCoordinate = null;

		try {
			newCoordinate = new Coordinate(getLength());
		} catch (LengthException e) {
			assert false : "Fatal programming error";
		}

		return add(other, newCoordinate);
	}

    /** 
     * Add this matrix with the other in the new vector provided.
     * 
     * @param other 	The other vector.
     * @param newVector The new vector.
     * @return The new vector. 
     */
	public Coordinate add(Vector other, Coordinate newCoordinate) 
			throws LengthException {

		checkForEqualLength(other);
		checkForEqualLength(newCoordinate);

		try {
			for (int i = 0; i < getLength() - 1; i++)
				newCoordinate.set(i, this.get(i) + other.get(i));
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return newCoordinate;
	}

    /** 
     * Add this matrix with the other in the new vector provided.
     * 
     * @param other 	The other vector.
     * @param newVector The new vector.
     * @return The new vector. 
     */
	public Coordinate add(Coordinate other, Coordinate newCoordinate) 
			throws LengthException {

		checkForEqualLength(other);
		checkForEqualLength(newCoordinate);

		try {
			for (int i = 0; i < getLength() - 1; i++)
				newCoordinate.set(i, this.get(i) + other.get(i));

		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return newCoordinate;
	}

	/**
	 * 
	 */
	public void copy(Vector vector) throws LengthException {

		checkForEqualLength(vector);

		try {
			for (int i = 0; i < getLength() - 1; i++)
				this.set(i, vector.get(i));
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}
	}

	/**
	 * 
	 */
	public void copy(Coordinate coordinate) throws LengthException {

		checkForEqualLength(coordinate);

		try {
			for (int i = 0; i < getLength() - 1; i++)
				this.set(i, coordinate.get(i));
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}
	}

	public double get(int index) {
		try {
			if (index == this.components.length)
				return 1.0;
			else {
				return this.components[index];
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new IndexOutOfBoundsException();
		}
	}

	public Coordinate.Iterator getIterator() {
		return this.new Iterator();
	}

	public int getLength() {
		return this.components.length + 1;
	}

    /** 
     * Compute dot this vector with the other vector 
     * 
     * @param other The other vector. 
     */
	public double dot(Coordinate other) throws LengthException {

		checkForEqualLength(other);

		double dotProduct = 0.0;

		try {
			for (int i = 0; i < getLength() - 1; i++)
				dotProduct += this.get(i) * other.get(i);
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return dotProduct;	
	}

    /** 
     * Compute dot this vector with the other vector 
     * 
     * @param other The other vector. 
     */
	public double dot(Vector other) throws LengthException {

		checkForEqualLength(other);

		double dotProduct = 0.0;

		try {
			for (int i = 0; i < getLength() - 1; i++)
				dotProduct += this.get(i) * other.get(i);
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return dotProduct;	
	}


	public double magnitude() {

		double magnitude = 0.0;
		double component = 0.0;

		try {
			for (int i = 0; i < getLength() - 1; i++) {
				component = this.get(i);
				magnitude += component * component;
			}
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return Math.sqrt(magnitude);
	} 

    /** 
     * Multiply this matrix by a scalar in a new matrix.
     * 
     * @param scalar The scalar to multiply by.
     * @return The new vector. 
     */
	public Coordinate mul(double scalar) {

		Coordinate newCoordinate = null;

		try {
			newCoordinate = new Coordinate(getLength());
			newCoordinate =  mul(scalar, newCoordinate);
		} catch (LengthException e) {
			assert false : "Fatal programming error";
		}

		return newCoordinate;
	}

    /** 
     * Multiply this matrix by a scalar in a new matrix.
     * 
     * @param scalar The scalar to multiply by.
     * @return The new vector. 
     */
	public Coordinate mul(double scalar, Coordinate newVector) 
			throws LengthException {

		try {
			for (int i = 0; i < getLength() - 1; i++)
				newVector.set(i, scalar * this.get(i));
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return newVector;
	}

	/**
	 *
	 */
	public Coordinate normalize() {

		Coordinate newVector = null;

		try {
			newVector = new Coordinate(getLength());
		} catch (LengthException e) {
			assert false : "Fatal programming error";
		}

		double magnitude = this.magnitude();

		try {
			for (int i = 0; i < getLength() - 1; i++)
				newVector.set(i, this.get(i) / magnitude);
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return newVector;
	}

	/**
	 * Set the value of the component at the given index.
	 * 
	 * The first value is at index 0.
	 *
	 * @param index  The index of the element.
	 * @param value  The new value.
	 */
	public void set(int index, double value) 
			throws IndexOutOfBoundsException {
		try {
			if (index == getLength() - 1) 
				return;
			else
				this.components[index] = value;
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new IndexOutOfBoundsException();
		}
	}

    /** 
     * Subtract this matrix from the other as a new vector.
     * 
     * @param other The other vector.
     * @return The new vector. 
     */
	public Coordinate sub(Coordinate other) throws LengthException {

		Coordinate newCoordinate = null;

		try {
			newCoordinate = new Coordinate(getLength());
		} catch (LengthException e) {
			assert false : "Fatal programming error";
		}

		return sub(other, newCoordinate);
	}

    /** 
     * Subtract this matrix from the other as a new vector.
     * 
     * @param other The other vector.
     * @return The new vector. 
     */
	public Coordinate sub(Vector other) throws LengthException {

		Coordinate newCoordinate = null;

		try {
			newCoordinate = new Coordinate(getLength());
		} catch (LengthException e) {
			assert false : "Fatal programming error";
		}

		return sub(other, newCoordinate);
	}

    /** 
     * Subtract this matrix from the other as a new vector.
     * 
     * @param other The other vector.
     * @return The new vector. 
     */
	public Coordinate sub(Coordinate other, Coordinate newCoordinate)
			throws LengthException {

		checkForEqualLength(other);
		checkForEqualLength(newCoordinate);

		try {
			for (int i = 0; i < getLength() - 1; i++)
				newCoordinate.set(i, this.get(i) - other.get(i));
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return newCoordinate;
	}

    /** 
     * Subtract this matrix from the other as a new vector.
     * 
     * @param other The other vector.
     * @return The new vector. 
     */
	public Coordinate sub(Vector other, Coordinate newCoordinate) 
			throws LengthException {

		checkForEqualLength(other);
		checkForEqualLength(newCoordinate);

		try {
			for (int i = 0; i < getLength() -1; i++)
				newCoordinate.set(i, this.get(i) - other.get(i));
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return newCoordinate;
	}

    /** 
     * Add this matrix with the other in place.
     * 
     * @param other The other vector.
     * @return This vector modified. 
     */
	public Coordinate iadd(Coordinate other) throws LengthException {

		checkForEqualLength(other);

		try {
			for (int i = 0; i < getLength() - 1; i++)
				this.set(i, this.get(i) + other.get(i));
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return this;
	}

    /** 
     * Add this matrix with the other in place.
     * 
     * @param other The other vector.
     * @return This vector modified. 
     */
	public Coordinate iadd(Vector other) throws LengthException {

		checkForEqualLength(other);

		try {
			for (int i = 0; i < getLength() - 1; i++)
				this.set(i, this.get(i) + other.get(i));
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return this;
	}

    /** 
     * Multiply this matrix by a scalar in place. 
     * 
     * @param scalar The scalar to multiply by.
     * @return The modified vector. 
     */
	public Coordinate imul(double scalar) {

		try {
			for (int i = 0; i < getLength() - 1; i++)
				this.set(i, scalar * this.get(i));
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return this;
	}

	public Coordinate inormalize() {
		double magnitude = this.magnitude();

		try {
			for (int i = 0; i < getLength() - 1; i++)
				this.set(i, this.get(i) / magnitude );
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return this;
	}	

    /** 
     * Subtract this matrix from the other in place.
     * 
     * @param other The other vector.
     * @return This vector modified. 
     */
	public Coordinate isub(Coordinate other) throws LengthException {

		checkForEqualLength(other);

		try {
			for (int i = 0; i < getLength() - 1; i++)
				this.set(i, this.get(i) - other.get(i));
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return this;
	}

    /** 
     * Subtract this matrix from the other in place.
     * 
     * @param other The other vector.
     * @return This vector modified. 
     */
	public Coordinate isub(Vector other) throws LengthException {

		checkForEqualLength(other);

		try {
			for (int i = 0; i < getLength() - 1; i++)
				this.set(i, this.get(i) - other.get(i));
		} catch (IndexOutOfBoundsException e) {
			assert false : "Fatal programming error";
		}

		return this;
	}

	/**
	 * Check that this and the other Coordinate are the same length.
	 *
	 * @param other The other Coordinate.
	 */
	protected void checkForEqualLength(Coordinate other) 
			throws LengthException {

		if (this.getLength() != other.getLength())
			throw new LengthException("Vectors must be of equal length");
	}

	/**
	 * Check that other vector are the same length is 1 shorter.
	 *
	 * @param other The other vector.
	 */
	protected void checkForEqualLength(Vector other) throws LengthException {

		if (this.getLength() != other.getLength())
			throw new LengthException("Vectors must be of equal length");
	}
}