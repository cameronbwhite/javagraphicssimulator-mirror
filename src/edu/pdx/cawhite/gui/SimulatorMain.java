package edu.pdx.cawhite.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
import edu.pdx.cawhite.simulator.Enviroment2D;
import edu.pdx.cawhite.simulator.Enviroment3D;
import edu.pdx.cawhite.simulator.Player2D;
import edu.pdx.cawhite.simulator.Player3D;
import edu.pdx.cawhite.simulator.Object2D;
import edu.pdx.cawhite.simulator.Object3D;
import edu.pdx.cawhite.simulator.Screen2D;
import edu.pdx.cawhite.simulator.Screen3D;
import edu.pdx.cawhite.simulator.Listener2D;
import edu.pdx.cawhite.simulator.Listener3D;
import edu.pdx.cawhite.math.coordinates.Coordinate;
import edu.pdx.cawhite.math.Vector;
import edu.pdx.cawhite.math.LengthException;

 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;


public class SimulatorMain extends JPanel implements ActionListener {

	protected JButton b1, b2, b3, b4;
	protected static JFrame frame;
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		JButton b = (JButton) arg0.getSource();
		if (b == b1) {
			singlePlayer2D();
			this.frame.setVisible(false);
        } else if (b == b2) {
            JOptionPane.showMessageDialog(frame, "Not implemented");
		} else if (b == b3) {
            singlePlayer3D();
            this.frame.setVisible(false);
		} else if (b == b4) {
			JOptionPane.showMessageDialog(frame, "Not implemented");
        }
	}

	public SimulatorMain() {
        b1 = new JButton("2D Single player");
        b1.addActionListener(this);
        
        b2 = new JButton("2D Duel player");
        b2.addActionListener(this);

        b3 = new JButton("3D Single player");
        b3.addActionListener(this);

        b4 = new JButton("3D Duel player");
        b4.addActionListener(this);

        setLayout(new GridLayout(2,2));
        add(b1);
        add(b2);
        add(b3);
        add(b4);
	}

	private static void createAndShowGUI() {
		frame = new JFrame("Java Graphics Simulator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        SimulatorMain newContentPane = new SimulatorMain();
        newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);

        frame.pack();
        frame.setVisible(true);
	}

    public static void singlePlayer2D() {
        JFrame frame = new JFrame("Java Simulator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(100, 100);

        Enviroment2D enviroment = new Enviroment2D();
        Player2D player = null;
        
        try {
            player = new Player2D(enviroment, new Coordinate(new double[] {100.0, 200.0}), 20.0);
            //new Object2D(enviroment, new Coordinate(new double[] {100.0, 200.0}), 50.0);
            //new Object2D(enviroment, new Coordinate(new double[] {40.0, 200.0}),  50.0);
            //new Object2D(enviroment, new Coordinate(new double[] {300.0, 200.0}), 50.0);
        } catch (LengthException e) {
            assert false;
        }
        Screen2D screen = player.getScreen();
        screen.addKeyListener(new Listener2D());
        
        frame.setSize(1000,1000);
        frame.add(screen);
        frame.pack();
        frame.setVisible(true);
    }

    public static void singlePlayer3D() {
    	Coordinate center = null;
    	Player3D player = null;
        JFrame frame = new JFrame("Java Simulator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Enviroment3D enviroment = new Enviroment3D();
        
        try {
            center = new Coordinate(new double[] {
                0.0, 0.0, -10.0
            });
            player = new Player3D(enviroment, center, 20.0);
            /*// x-axis
            new Object3D(enviroment,  Functions3D.line(
                         new Coordinate(new double[] {0.0, 500.0, 500.0}), 
                         new Coordinate(new double[] {1.0,   0.0,   0.0}),
                         0.0, 1000.0, 1.0));
            // y-axis
    
            new Object3D(enviroment, Functions3D.line(
                         new Coordinate(new double[] {500.0, 0.0, 500.0}), 
                         new Coordinate(new double[] {0.0,   1.0,   0.0}),
                         0.0, 1000.0, 1.0));
            // z-axis
            new Object3D(enviroment, Functions3D.line(
                         new Coordinate(new double[] {500.0, 500.0, 0.0}), 
                         new Coordinate(new double[] {0.0,   0.0,   1.0}),
                         0.0, 1000.0, 1.0));
                         */
        } catch (LengthException e) {}
        Screen3D screen = player.getScreen();
        screen.addKeyListener(new Listener3D());
        player.print();
        
        frame.setSize(1000,1000);
        frame.add(screen);
        frame.setVisible(true);
        frame.pack();
    }

	public static void main(String[] args) {
	    //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
	}

}
