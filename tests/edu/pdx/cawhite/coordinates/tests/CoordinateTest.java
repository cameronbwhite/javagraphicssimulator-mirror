/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.coordinates.tests;

import static org.junit.Assert.*;
import org.junit.Test;

import edu.pdx.cawhite.math.coordinates.Coordinate;
import edu.pdx.cawhite.math.LengthException;

public class CoordinateTest {

	@Test
	public void testConstructor1() {
		try {
			Coordinate vector = new Coordinate(1);
			assertTrue("The length of the vector should be 1", vector.getLength() == 1);
			assertTrue("The first item should be 0", vector.get(0) == 1.0);
		} catch (LengthException e) {
			fail();
		} catch (IndexOutOfBoundsException e) {
			fail();
		}
	}
	
	@Test
	public void testConstructor2() {
		try {
			Coordinate vector = new Coordinate(2);
			assertTrue("The length of the vector should be 2", vector.getLength() == 2);
			assertTrue("The first item should be 0.", vector.get(0) == 0);
			assertTrue("The second item should be 0.", vector.get(1) == 1.0);
		} catch (LengthException e) {
			fail();
		} catch (IndexOutOfBoundsException e) {
			fail();
		}
	}

	@Test
	public void testConstructor3() {
		try {
			Coordinate vector = new Coordinate(-1);
		} catch (LengthException e) {
			return;
		}
		fail("LengthException should occur.");
	}

	@Test
	public void testSetGet1() {
		try {
			Coordinate vector = new Coordinate(new double[] {1.0, 2.0, 3.0});
			assertTrue("The length of the vector should be 3", 
				vector.getLength() == 4);
			assertTrue("The 1st item should be 1.0",
						vector.get(0) == 1.0);
			assertTrue("The 2nd item should be 2.0",
						vector.get(1) == 2.0);
			assertTrue("The 3rd item should be 3.0",
						vector.get(2) == 3.0);
			assertTrue("The 4th item should be 1.0",
						vector.get(3) == 1.0);
			vector.set(0, 1.1);
			vector.set(1, 2.2);
			vector.set(2, 3.3);
			assertTrue("The 1st item should be 1.0",
						vector.get(0) == 1.1);
			assertTrue("The 2nd item should be 2.0",
						vector.get(1) == 2.2);
			assertTrue("The 3rd item should be 3.0",
						vector.get(2) == 3.3);
			assertTrue("The 4th item should be 1.0",
						vector.get(3) == 1.0);
		} catch (IndexOutOfBoundsException e) {
			fail();
		} catch (LengthException e) {
			fail();
		}
	}
	
	@Test
	public void testSetGet2() {
		try {
			Coordinate vector = new Coordinate(4);
			assertTrue("The length of the vector should be 4", vector.getLength() == 4);
			assertTrue("The 1st item should be 0", vector.get(0) == 0);
			assertTrue("The 2nd item should be 0", vector.get(1) == 0);
			assertTrue("The 3rd item should be 0", vector.get(2) == 0);
			assertTrue("The 4th item should be 0", vector.get(3) == 1.0);
			vector.set(0, 1.1);
			vector.set(1, 2.2);
			vector.set(2, 3.3);
			assertTrue("The length of the vector should be 4", vector.getLength() == 4);
			assertTrue("The 1st item should be 0", vector.get(0) == 1.1);
			assertTrue("The 2nd item should be 0", vector.get(1) == 2.2);
			assertTrue("The 3rd item should be 0", vector.get(2) == 3.3);	
			assertTrue("The 4th item should be 1.0", vector.get(3) == 1.0);	
		} catch (IndexOutOfBoundsException e) {
			fail();
		} catch (LengthException e) {
			fail();
		}
	}

	@Test()
	public void testGetException1() {
		try {
			Coordinate vector = new Coordinate(new double[] {1.0, 2.0, 3.0});
			vector.get(-1);
		} catch (LengthException e) {
			fail();
		} catch (IndexOutOfBoundsException e) {
			return;
		}
		fail("IndexOutOfBoundsException should of occured.");
	}

	@Test()
	public void testGetException2() {
		try {
			Coordinate vector = new Coordinate(new double[] {1.0, 2.0, 3.0});
			vector.get(4);
		} catch (LengthException e) {
			fail();
		} catch (IndexOutOfBoundsException e) {
			return;
		}
		fail("IndexOutOfBoundsException should of occured.");
	}

	@Test()
	public void testSetException1() {
		try {
			Coordinate vector = new Coordinate(new double[] {1.0, 2.0, 3.0});
			vector.set(-1, 1.1);
		} catch (LengthException e) {
			fail();
		} catch (IndexOutOfBoundsException e) {
			return;
		}
		fail("IndexOutOfBoundsException should of occured.");
	}

	@Test()
	public void testSetException2() {
		try {
			Coordinate vector = new Coordinate(new double[] {1.0, 2.0, 3.0});
			vector.set(4, 1.1);
		} catch (LengthException e) {
			fail();
		} catch (IndexOutOfBoundsException e) {
			return;
		}
		fail("IndexOutOfBoundsException should of occured.");
	}
	
	@Test
	public void testAdd() {
		try {
			Coordinate vector0 = new Coordinate(4);
			Coordinate vector1 = new Coordinate(4);
			vector0.set(0, 1.0);
			vector0.set(1, 2.0);
			vector0.set(2, 3.0);
			vector1.set(0, 4.0);
			vector1.set(1, 5.0);
			vector1.set(2, 6.0);
			Coordinate newCoordinate = vector0.add(vector1);
			assertTrue("The length of the vector should be 4", newCoordinate.getLength() == 4);
			assertTrue("The 1st item should be 5.0.", newCoordinate.get(0) == 5.0);
			assertTrue("The 2nd item should be 7.0.", newCoordinate.get(1) == 7.0);
			assertTrue("The 3rd item should be 9.0.", newCoordinate.get(2) == 9.0);
			assertTrue("The 4th item should be 1.0",  newCoordinate.get(3) == 1.0);	
		} catch (IndexOutOfBoundsException e) {
			fail();
		} catch (LengthException e) {
			fail();
		}
	}
	
	@Test
	public void testIAdd() {
		try {
			Coordinate vector0 = new Coordinate(4);
			Coordinate vector1 = new Coordinate(4);
			vector0.set(0, 1.0);
			vector0.set(1, 2.0);
			vector0.set(2, 3.0);
			vector1.set(0, 4.0);
			vector1.set(1, 5.0);
			vector1.set(2, 6.0);
			vector0.iadd(vector1);
			assertTrue("The length of the vector should be 4", vector0.getLength() == 4);
			assertTrue("The 1st item should be 5.0.", vector0.get(0) == 5.0);
			assertTrue("The 2nd item should be 7.0.", vector0.get(1) == 7.0);
			assertTrue("The 3rd item should be 9.0.", vector0.get(2) == 9.0);
			assertTrue("The 4th item should be 1.0",  vector0.get(3) == 1.0);	
		} catch (IndexOutOfBoundsException e) {
			fail();
		} catch (LengthException e) {
			fail();
		}
	}
	
	@Test
	public void testSub() {
		try {
			Coordinate vector0 = new Coordinate(4);
			Coordinate vector1 = new Coordinate(4);
			vector0.set(0, 1.0);
			vector0.set(1, 2.0);
			vector0.set(2, 3.0);
			vector1.set(0, 4.0);
			vector1.set(1, 5.0);
			vector1.set(2, 6.0);
			Coordinate newCoordinate = vector0.sub(vector1);
			assertTrue("The length of the vector should be 4", newCoordinate.getLength() == 4);
			assertTrue("The 1st item should be -3.0.", newCoordinate.get(0) == -3.0);
			assertTrue("The 2nd item should be -3.0.", newCoordinate.get(1) == -3.0);
			assertTrue("The 3rd item should be -3.0.", newCoordinate.get(2) == -3.0);
			assertTrue("The 4th item should be 1.0.", newCoordinate.get(3) == 1.0);
		} catch (IndexOutOfBoundsException e) {
			fail();
		} catch (LengthException e) {
			fail();
		}
	}
	
	@Test
	public void testISub() {
		try {
			Coordinate vector0 = new Coordinate(4);
			Coordinate vector1 = new Coordinate(4);
			vector0.set(0, 1.0);
			vector0.set(1, 2.0);
			vector0.set(2, 3.0);
			vector1.set(0, 4.0);
			vector1.set(1, 5.0);
			vector1.set(2, 6.0);
			vector0.isub(vector1);
			assertTrue("The length of the vector should be 4", vector0.getLength() == 4);
			assertTrue("The 1st item should be -3.0.", vector0.get(0) == -3.0);
			assertTrue("The 2nd item should be -3.0.", vector0.get(1) == -3.0);
			assertTrue("The 3rd item should be -3.0.", vector0.get(2) == -3.0);
			assertTrue("The 4th item should be 1.0.", vector0.get(3) == 1.0);
		} catch (IndexOutOfBoundsException e) {
			fail();
		} catch (LengthException e) {
			fail();
		}
	}
	
	@Test
	public void testMul() {
		try {
			Coordinate vector = new Coordinate(4);
			vector.set(0, 1.0);
			vector.set(1, 2.0);
			vector.set(2, 3.0);
			assertTrue("The length of the vector should be 4", vector.getLength() == 4);
			assertTrue("The 1st item should be 1.0.", vector.get(0) == 1.0);
			assertTrue("The 2nd item should be 2.0.", vector.get(1) == 2.0);
			assertTrue("The 3rd item should be 3.0.", vector.get(2) == 3.0);
			assertTrue("The 4th item should be 1.0.", vector.get(3) == 1.0);
			Coordinate newCoordinate = vector.mul(3.0);
			assertTrue("The length of the vector should be 3", newCoordinate.getLength() == 4);
			assertTrue("The 1st item should be 1.0.", newCoordinate.get(0) == 3.0);
			assertTrue("The 2nd item should be 2.0.", newCoordinate.get(1) == 6.0);
			assertTrue("The 3rd item should be 3.0.", newCoordinate.get(2) == 9.0);
			assertTrue("The 4th item should be 1.0.", vector.get(3) == 1.0);
		} catch (IndexOutOfBoundsException e) {
			fail();
		} catch (LengthException e) {
			fail();
		}
	}
	
	@Test
	public void testIMul() {
		try {
			Coordinate vector = new Coordinate(4);
			vector.set(0, 1.0);
			vector.set(1, 2.0);
			vector.set(2, 3.0);
			assertTrue("The length of the vector should be 4", vector.getLength() == 4);
			assertTrue("The 1st item should be 1.0.", vector.get(0) == 1.0);
			assertTrue("The 2nd item should be 2.0.", vector.get(1) == 2.0);
			assertTrue("The 3rd item should be 3.0.", vector.get(2) == 3.0);
			assertTrue("The 4th item should be 1.0.", vector.get(3) == 1.0);
			vector.imul(3.0);
			assertTrue("The length of the vector should be 3", vector.getLength() == 4);
			assertTrue("The 1st item should be 1.0.", vector.get(0) == 3.0);
			assertTrue("The 2nd item should be 2.0.", vector.get(1) == 6.0);
			assertTrue("The 3rd item should be 3.0.", vector.get(2) == 9.0);
			assertTrue("The 4th item should be 1.0.", vector.get(3) == 1.0);
		} catch (IndexOutOfBoundsException e) {
			fail();
		} catch (LengthException e) {
			fail();
		}
	}
	
	@Test
	public void testDot() {
		try {
			Coordinate vector0 = new Coordinate(4);
			Coordinate vector1 = new Coordinate(4);
			vector0.set(0, 1.0);
			vector0.set(1, 2.0);
			vector0.set(2, 3.0);
			vector1.set(0, 4.0);
			vector1.set(1, 5.0);
			vector1.set(2, 6.0);
			assertTrue("The length of the vector should be 4", vector0.getLength() == 4);
			assertTrue("The 1st item should be 1.0.", vector0.get(0) == 1.0);
			assertTrue("The 2nd item should be 2.0.", vector0.get(1) == 2.0);
			assertTrue("The 3rd item should be 3.0.", vector0.get(2) == 3.0);
			assertTrue("The 4th item should be 1.0.", vector0.get(3) == 1.0);
			assertTrue("The length of the vector should be 4", vector1.getLength() == 4);
			assertTrue("The 1st item should be 1.0.", vector1.get(0) == 4.0);
			assertTrue("The 2nd item should be 2.0.", vector1.get(1) == 5.0);
			assertTrue("The 3rd item should be 3.0.", vector1.get(2) == 6.0);
			assertTrue("The 4th item should be 1.0.", vector1.get(3) == 1.0);
			Double dotProduct = vector0.dot(vector1);
			assertTrue("The dot product should be 32.0.", dotProduct == 32.0);
			assertTrue("The length of the vector should be 4", vector0.getLength() == 4);
			assertTrue("The 1st item should be 1.0.", vector0.get(0) == 1.0);
			assertTrue("The 2nd item should be 2.0.", vector0.get(1) == 2.0);
			assertTrue("The 3rd item should be 3.0.", vector0.get(2) == 3.0);
			assertTrue("The 4th item should be 1.0.", vector0.get(3) == 1.0);
			assertTrue("The length of the vector should be 4", vector1.getLength() == 4);
			assertTrue("The 1st item should be 1.0.", vector1.get(0) == 4.0);
			assertTrue("The 2nd item should be 2.0.", vector1.get(1) == 5.0);
			assertTrue("The 3rd item should be 3.0.", vector1.get(2) == 6.0);
			assertTrue("The 4th item should be 1.0.", vector1.get(3) == 1.0);
		} catch (IndexOutOfBoundsException e) {
			fail();
		} catch (LengthException e) {
			fail();
		}
	}

	@Test
	public void testCopy() {
		try {
			Coordinate v1 = new Coordinate(new double[] {1.0, 2.0, 3.0});
			Coordinate v2 = new Coordinate(4);
			v2.copy(v1);
			assertTrue("The 1st item should be 1.0.", 
						v2.get(0) == 1.0);
			assertTrue("The 2nd item should be 2.0.", 
						v2.get(1) == 2.0);
			assertTrue("The 3rd item should be 3.0.", 
						v2.get(2) == 3.0);
			assertTrue("The 4th item should be 1.0.", 
						v2.get(3) == 1.0);
		} catch (IndexOutOfBoundsException e) {
			fail();
		} catch (LengthException e) {
			fail();
		}
	}

	@Test
	public void testCopyException() {
		try {
			Coordinate v1 = new Coordinate(new double[] {1.0, 2.0, 3.0});
			Coordinate v2 = new Coordinate(2);
			assertTrue("The length of the vector should be 3", 
						v1.getLength() == 4);
			assertTrue("The length of the vector should be 2", 
						v2.getLength() == 2);
			v2.copy(v1);
		} catch (LengthException e) {
			return;
		}
		fail("LengthException should of occured.");
	}

	@Test
	public void testMagnitude() {
		try {
			Coordinate v = new Coordinate(new double[] {1.0, 1.0, 1.0, 1.0});
			double magnitude = v.magnitude();
			assertTrue("The magnitude shoulde be 2.0", magnitude == 2.0);
		} catch (LengthException e) {
			fail();
		}
	}

	@Test
	public void testNormalize() {
		try {
			Coordinate v1 = new Coordinate(new double[] {1.0, 1.0, 1.0, 1.0});
			Coordinate v2 = v1.normalize();
			assertTrue("The 1st element should be 1/2", v2.get(0) == 1.0/2.0);
			assertTrue("The 2nd element should be 1/2", v2.get(1) == 1.0/2.0);
			assertTrue("The 3rd element should be 1/2", v2.get(2) == 1.0/2.0);
			assertTrue("The 4th element should be 1/2", v2.get(3) == 1.0/2.0);
		} catch (LengthException e) {
			fail();
		} catch (IndexOutOfBoundsException e) {
			fail();
		}
	}

	@Test
	public void testINormalize() {
		try {
			Coordinate v1 = new Coordinate(new double[] {1.0, 1.0, 1.0, 1.0});
			v1.inormalize();
			assertTrue("The 1st element should be 1/2", v1.get(0) == 1.0/2.0);
			assertTrue("The 2nd element should be 1/2", v1.get(1) == 1.0/2.0);
			assertTrue("The 3rd element should be 1/2", v1.get(2) == 1.0/2.0);
			assertTrue("The 4th element should be 1/2", v1.get(3) == 1.0/2.0);
		} catch (LengthException e) {
			fail();
		} catch (IndexOutOfBoundsException e) {
			fail();
		}
	}
}
