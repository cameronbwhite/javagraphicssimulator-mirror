package edu.pdx.cawhite.coordinates.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.pdx.cawhite.math.coordinates.CoordinateMatrix;
import edu.pdx.cawhite.math.transformations.TransformationMatrix;
import edu.pdx.cawhite.math.MatrixSizeException;

public class TransformationMatrixTest {

	@Test
	public void testMul1() {
		CoordinateMatrix newMatrix = null;
		
		TransformationMatrix transformation = 
			new TransformationMatrix(new double[][] {
				{2.0},
				{0.0},
		});

		CoordinateMatrix coordinates =
			new CoordinateMatrix(new double [][] {
				{2.0},
				{3.0},
		});

		try {
			newMatrix = transformation.mul(coordinates);
		} catch (MatrixSizeException e) {
			fail();
		}

		assertTrue("There should be 2 rows",transformation.getNumberOfRows() == 2);		
		assertTrue("There should be 1 column", transformation.getNumberOfColumns() == 1);
		assertTrue("Item (0,0) should be 2.0", transformation.get(0,0) == 2.0);
		assertTrue("Item (1,0) should be 0.0", transformation.get(1,0) == 0.0);

		assertTrue("There should be 2 rows", coordinates.getNumberOfRows() == 2);		
		assertTrue("There should be 2 column", coordinates.getNumberOfColumns() == 2);
		assertTrue("Item (0,0) should be 2.0", coordinates.get(0,0) == 2.0);
		assertTrue("Item (0,1) should be 3.0", coordinates.get(0,1) == 3.0);
		assertTrue("Item (1,0) should be 1.0", coordinates.get(1,0) == 1.0);
		assertTrue("Item (1,1) should be 1.0", coordinates.get(1,1) == 1.0);

		assertTrue("There should be 2 rows", newMatrix.getNumberOfRows() == 2);		
		assertTrue("There should be 2 column", newMatrix.getNumberOfColumns() == 2);
		assertTrue("Item (0,0) should be 4.0", newMatrix.get(0,0) == 4.0);
		assertTrue("Item (0,1) should be 6.0", newMatrix.get(0,1) == 6.0);
		assertTrue("Item (1,0) should be 1.0", newMatrix.get(1,0) == 1.0);
		assertTrue("Item (1,1) should be 1.0", newMatrix.get(1,1) == 1.0);
	}

	@Test
	public void testMul2() {
		CoordinateMatrix newMatrix = null;
		
		TransformationMatrix transformation = 
			new TransformationMatrix(new double[][] {
				{2.0, 0.0, 0.0},
				{0.0, 3.0, 0.0},
				{0.0, 0.0, 1.0}
		});

		CoordinateMatrix coordinates =
			new CoordinateMatrix(new double [][] {
				{2.0, 4.0},
				{3.0, 6.0},
		});

		try {
			newMatrix = transformation.mul(coordinates);
		} catch (MatrixSizeException e) {
			fail();
		}

		assertTrue("There should be 3 rows",transformation.getNumberOfRows() == 3);		
		assertTrue("There should be 3 column", transformation.getNumberOfColumns() == 3);
		assertTrue("Item (0,0) should be 2.0", transformation.get(0,0) == 2.0);
		assertTrue("Item (0,1) should be 0.0", transformation.get(0,1) == 0.0);
		assertTrue("Item (0,2) should be 0.0", transformation.get(0,2) == 0.0);
		assertTrue("Item (1,0) should be 0.0", transformation.get(1,0) == 0.0);
		assertTrue("Item (1,1) should be 3.0", transformation.get(1,1) == 3.0);
		assertTrue("Item (1,2) should be 0.0", transformation.get(1,2) == 0.0);
		assertTrue("Item (2,0) should be 0.0", transformation.get(2,0) == 0.0);
		assertTrue("Item (2,1) should be 0.0", transformation.get(2,1) == 0.0);
		assertTrue("Item (2,2) should be 1.0", transformation.get(2,2) == 1.0);

		assertTrue("There should be 3 rows", coordinates.getNumberOfRows() == 3);		
		assertTrue("There should be 2 column", coordinates.getNumberOfColumns() == 2);
		assertTrue("Item (0,0) should be 2.0", coordinates.get(0,0) == 2.0);
		assertTrue("Item (0,1) should be 3.0", coordinates.get(0,1) == 3.0);
		assertTrue("Item (1,0) should be 4.0", coordinates.get(1,0) == 4.0);
		assertTrue("Item (1,1) should be 6.0", coordinates.get(1,1) == 6.0);
		assertTrue("Item (2,0) should be 1.0", coordinates.get(2,0) == 1.0);
		assertTrue("Item (2,1) should be 1.0", coordinates.get(2,1) == 1.0);

		assertTrue("There should be 3 rows", newMatrix.getNumberOfRows() == 3);		
		assertTrue("There should be 2 column", newMatrix.getNumberOfColumns() == 2);
		assertTrue("Item (0,0) should be 2.0", newMatrix.get(0,0) == 4.0);
		assertTrue("Item (0,1) should be 3.0", newMatrix.get(0,1) == 6.0);
		assertTrue("Item (1,0) should be 4.0", newMatrix.get(1,0) == 12.0);
		assertTrue("Item (1,1) should be 6.0", newMatrix.get(1,1) == 18.0);
		assertTrue("Item (2,0) should be 1.0", newMatrix.get(2,0) == 1.0);
		assertTrue("Item (2,1) should be 1.0", newMatrix.get(2,1) == 1.0);
	}

	@Test
	public void testPMul2() {
		CoordinateMatrix newMatrix = null;
		
		TransformationMatrix transformation = 
			new TransformationMatrix(new double[][] {
	            {1.0, 0.0,   0.0, 0.0},
	            {0.0, 1.0,   0.0, 0.0},
	            {0.0, 0.0,   0.0, 0.0},
	            {0.0, 0.0, -1.0/10, 1.0},
		});

		CoordinateMatrix coordinates =
			new CoordinateMatrix(new double [][] {
				{3.0, 1.0, 5.0},
				{5.0, 1.0, 5.0},
				{5.0, 0.0, 5.0},
				{3.0, 0.0, 5.0},
				{3.0, 1.0, 4.0},
				{5.0, 1.0, 4.0},
				{5.0, 0.0, 4.0},
				{3.0, 0.0, 4.0},
		});

		try {
			newMatrix = transformation.pMul(coordinates);
		} catch (MatrixSizeException e) {
			fail();
		}

		assertTrue("There should be 3 rows",transformation.getNumberOfRows() == 4);		
		assertTrue("There should be 3 column", transformation.getNumberOfColumns() == 4);
		assertTrue("Item (0,0) should be 1.0", transformation.get(0,0) == 1.0);
		assertTrue("Item (0,1) should be 0.0", transformation.get(0,1) == 0.0);
		assertTrue("Item (0,2) should be 0.0", transformation.get(0,2) == 0.0);
		assertTrue("Item (0,3) should be 0.0", transformation.get(0,3) == 0.0);
		assertTrue("Item (1,0) should be 0.0", transformation.get(1,0) == 0.0);
		assertTrue("Item (1,1) should be 1.0", transformation.get(1,1) == 1.0);
		assertTrue("Item (1,2) should be 0.0", transformation.get(1,2) == 0.0);
		assertTrue("Item (1,3) should be 0.0", transformation.get(1,3) == 0.0);
		assertTrue("Item (2,0) should be 0.0", transformation.get(2,0) == 0.0);
		assertTrue("Item (2,1) should be 0.0", transformation.get(2,1) == 0.0);
		assertTrue("Item (2,2) should be 0.0", transformation.get(2,2) == 0.0);
		assertTrue("Item (2,3) should be 0.0", transformation.get(2,3) == 0.0);
		assertTrue("Item (3,0) should be 0.0", transformation.get(3,0) == 0.0);
		assertTrue("Item (3,1) should be 0.0", transformation.get(3,1) == 0.0);
		assertTrue("Item (3,2) should be -0.1", transformation.get(3,2) == -0.1);
		assertTrue("Item (3,3) should be 0.0", transformation.get(3,3) == 1.0);

		assertTrue("There should be 4 rows", coordinates.getNumberOfRows() == 4);		
		assertTrue("There should be 8 column", coordinates.getNumberOfColumns() == 8);
		assertTrue("Item (0,0) should be 2.0", coordinates.get(0,0) == 3.0);
		assertTrue("Item (1,0) should be 3.0", coordinates.get(1,0) == 1.0);
		assertTrue("Item (2,0) should be 4.0", coordinates.get(2,0) == 5.0);
		assertTrue("Item (3,0) should be 6.0", coordinates.get(3,0) == 1.0);
		assertTrue("Item (0,1) should be 2.0", coordinates.get(0,1) == 5.0);
		assertTrue("Item (1,1) should be 3.0", coordinates.get(1,1) == 1.0);
		assertTrue("Item (2,1) should be 4.0", coordinates.get(2,1) == 5.0);
		assertTrue("Item (3,1) should be 6.0", coordinates.get(3,1) == 1.0);
		assertTrue("Item (0,2) should be 2.0", coordinates.get(0,2) == 5.0);
		assertTrue("Item (1,2) should be 3.0", coordinates.get(1,2) == 0.0);
		assertTrue("Item (2,2) should be 4.0", coordinates.get(2,2) == 5.0);
		assertTrue("Item (3,2) should be 6.0", coordinates.get(3,2) == 1.0);
		assertTrue("Item (0,3) should be 2.0", coordinates.get(0,3) == 3.0);
		assertTrue("Item (1,3) should be 3.0", coordinates.get(1,3) == 0.0);
		assertTrue("Item (2,3) should be 4.0", coordinates.get(2,3) == 5.0);
		assertTrue("Item (3,3) should be 6.0", coordinates.get(3,3) == 1.0);
		assertTrue("Item (0,4) should be 2.0", coordinates.get(0,4) == 3.0);
		assertTrue("Item (1,4) should be 3.0", coordinates.get(1,4) == 1.0);
		assertTrue("Item (2,4) should be 4.0", coordinates.get(2,4) == 4.0);
		assertTrue("Item (3,4) should be 6.0", coordinates.get(3,4) == 1.0);
		assertTrue("Item (0,5) should be 2.0", coordinates.get(0,5) == 5.0);
		assertTrue("Item (1,5) should be 3.0", coordinates.get(1,5) == 1.0);
		assertTrue("Item (2,5) should be 4.0", coordinates.get(2,5) == 4.0);
		assertTrue("Item (3,5) should be 6.0", coordinates.get(3,5) == 1.0);
		assertTrue("Item (0,6) should be 2.0", coordinates.get(0,6) == 5.0);
		assertTrue("Item (1,6) should be 3.0", coordinates.get(1,6) == 0.0);
		assertTrue("Item (2,6) should be 4.0", coordinates.get(2,6) == 4.0);
		assertTrue("Item (3,6) should be 6.0", coordinates.get(3,6) == 1.0);
		assertTrue("Item (0,7) should be 2.0", coordinates.get(0,7) == 3.0);
		assertTrue("Item (1,7) should be 3.0", coordinates.get(1,7) == 0.0);
		assertTrue("Item (2,7) should be 4.0", coordinates.get(2,7) == 4.0);
		assertTrue("Item (3,7) should be 6.0", coordinates.get(3,7) == 1.0);

		assertTrue("There should be 4 rows", newMatrix.getNumberOfRows() == 4);		
		assertTrue("There should be 8 column", newMatrix.getNumberOfColumns() == 8);
		assertTrue("Item (0,0) should be 2.0", newMatrix.get(0,0) == 6.0);
		assertTrue("Item (1,0) should be 3.0", newMatrix.get(1,0) == 2.0);
		assertTrue("Item (2,0) should be 4.0", newMatrix.get(2,0) == 0.0);
		assertTrue("Item (3,0) should be 6.0", newMatrix.get(3,0) == 1.0);
		assertTrue("Item (0,1) should be 2.0", newMatrix.get(0,1) == 10.0);
		assertTrue("Item (1,1) should be 3.0", newMatrix.get(1,1) == 2.0);
		assertTrue("Item (2,1) should be 4.0", newMatrix.get(2,1) == 0.0);
		assertTrue("Item (3,1) should be 6.0", newMatrix.get(3,1) == 1.0);
		assertTrue("Item (0,2) should be 2.0", newMatrix.get(0,2) == 10.0);
		assertTrue("Item (1,2) should be 3.0", newMatrix.get(1,2) == 0.0);
		assertTrue("Item (2,2) should be 4.0", newMatrix.get(2,2) == 0.0);
		assertTrue("Item (3,2) should be 6.0", newMatrix.get(3,2) == 1.0);
		assertTrue("Item (0,3) should be 2.0", newMatrix.get(0,3) == 6.0);
		assertTrue("Item (1,3) should be 3.0", newMatrix.get(1,3) == 0.0);
		assertTrue("Item (2,3) should be 4.0", newMatrix.get(2,3) == 0.0);
		assertTrue("Item (3,3) should be 6.0", newMatrix.get(3,3) == 1.0);
		assertTrue("Item (0,4) should be 5.0", newMatrix.get(0,4) == 5.0);
		assertTrue("Item (1,4) should be 1.7", newMatrix.get(1,4) == 1.0 + (2.0/3.0));
		assertTrue("Item (2,4) should be 0.0", newMatrix.get(2,4) == 0.0);
		assertTrue("Item (3,4) should be 1.0", newMatrix.get(3,4) == 1.0);
		assertTrue("Item (0,5) should be 2.0", newMatrix.get(0,5) == 8.0 + (1.0/3.0));
		assertTrue("Item (1,5) should be 3.0", newMatrix.get(1,5) == 1.0 + (2.0/3.0));
		assertTrue("Item (2,5) should be 4.0", newMatrix.get(2,5) == 0.0);
		assertTrue("Item (3,5) should be 6.0", newMatrix.get(3,5) == 1.0);
		assertTrue("Item (0,6) should be 2.0", newMatrix.get(0,6) == 8.0 + (1.0/3.0));
		assertTrue("Item (1,6) should be 3.0", newMatrix.get(1,6) == 0.0);
		assertTrue("Item (2,6) should be 4.0", newMatrix.get(2,6) == 0.0);
		assertTrue("Item (3,6) should be 6.0", newMatrix.get(3,6) == 1.0);
		assertTrue("Item (0,7) should be 2.0", newMatrix.get(0,7) == 5.0);
		assertTrue("Item (1,7) should be 3.0", newMatrix.get(1,7) == 0.0);
		assertTrue("Item (2,7) should be 4.0", newMatrix.get(2,7) == 0.0);
		assertTrue("Item (3,7) should be 6.0", newMatrix.get(3,7) == 1.0);
	}

}
