/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.containers.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.pdx.cawhite.containers.DList;

/**
 * @author Cameron Brandon White
 */
public class DListTest {
	
	@Test
	public void testConstructor() {
		DList<Integer> list = new DList<Integer>();
		assertNull("The front pointer should be null", list.getFront());
		assertNull("The back pointer should be null", list.getBack());
		assertTrue("The getLength should be 0.", list.getLength() == 0);
	}
	
	@Test
	public void testSingleAppend() {
		DList<Integer> list = new DList<Integer>();
		list.append(1);
		assertNotNull("The front should not be null", list.getFront());
		assertNotNull("The back should not be null", list.getBack());
		assertTrue("The front and back should be the same object", 
				   list.getFront() == list.getBack());
		assertTrue("The getLength should be 1", list.getLength() == 1);
		assertTrue("The 1st item should be 1", list.get(0) == 1);
	}
	
	@Test
	public void testSinglePrepend() {
		DList<Integer> list = new DList<Integer>();
		list.prepend(1);
		assertNotNull("The front should not be null", list.getFront());
		assertNotNull("The back should not be null", list.getBack());
		assertTrue("The front and back should be the same object", 
				   list.getFront() == list.getBack());
		assertTrue("The getLength should be 1", list.getLength() == 1);
		assertTrue("The 1st item should be 1", list.get(0) == 1);
	}
	
	@Test
	public void testMultipleAppend() {
		DList<Integer> list = new DList<Integer>();
		list.append(1);
		assertNotNull("The front should not be null", list.getFront());
		assertNotNull("The back should not be null", list.getBack());
		assertTrue("The front and back should be the same object", 
				   list.getFront() == list.getBack());
		assertTrue("The getLength should be 1", list.getLength() == 1);
		assertTrue("The 1st item should be 1", list.get(0) == 1);
		list.append(2);
		assertNotNull("The front should not be null", list.getFront());
		assertNotNull("The back should not be null", list.getBack());
		assertTrue("The front and back should not be the same object", 
				   list.getFront() != list.getBack());
		assertTrue("The getLength should be 2", list.getLength() == 2);
		assertTrue("The 1st item should be 1", list.get(0) == 1);
		assertTrue("The 2nd item should be 2", list.get(1) == 2);
		list.append(3);
		assertNotNull("The front should not be null", list.getFront());
		assertNotNull("The back should not be null", list.getBack());
		assertTrue("The front and back should not be the same object", 
				   list.getFront() != list.getBack());
		assertTrue("The getLength should be 2", list.getLength() == 3);
		assertTrue("The 1st item should be 1", list.get(0) == 1);
		assertTrue("The 2nd item should be 2", list.get(1) == 2);
		assertTrue("The 3rd item should be 3", list.get(2) == 3);
	}
	
	@Test
	public void testMultiplePrepend() {
		DList<Integer> list = new DList<Integer>();
		list.prepend(1);
		assertNotNull("The front should not be null", list.getFront());
		assertNotNull("The back should not be null", list.getBack());
		assertTrue("The front and back should be the same object", 
				   list.getFront() == list.getBack());
		assertTrue("The getLength should be 1", list.getLength() == 1);
		assertTrue("The 1st item should be 1", list.get(0) == 1);
		list.prepend(2);
		assertNotNull("The front should not be null", list.getFront());
		assertNotNull("The back should not be null", list.getBack());
		assertTrue("The front and back should not be the same object", 
				   list.getFront() != list.getBack());
		assertTrue("The getLength should be 2", list.getLength() == 2);
		assertTrue("The 1st item should be 2", list.get(0) == 2);
		assertTrue("The 2nd item should be 1", list.get(1) == 1);
		list.prepend(3);
		assertNotNull("The front should not be null", list.getFront());
		assertNotNull("The back should not be null", list.getBack());
		assertTrue("The front and back should not be the same object", 
				   list.getFront() != list.getBack());
		assertTrue("The getLength should be 2", list.getLength() == 3);
		assertTrue("The 1st item should be 3", list.get(0) == 3);
		assertTrue("The 2nd item should be 2", list.get(1) == 2);
		assertTrue("The 3rd item should be 1", list.get(2) == 1);
	}
	@Test
	public void testDeleteFrontOfThree() {
		DList<Integer> list = new DList<Integer>();
		list.append(1);
		list.append(2);
		list.append(3);
		list.delete(0);
		assertTrue("The list should be of getLength 2", list.getLength() == 2);
		assertTrue("The first item should be 2", list.get(0) == 2);
		assertTrue("The second item should be 3", list.get(1) == 3);
	}
	
	@Test
	public void testDeleteMiddleOfThree() {
		DList<Integer> list = new DList<Integer>();
		list.append(1);
		list.append(2);
		list.append(3);
		list.delete(1);
		assertTrue("The list should be of getLength 2", list.getLength() == 2);
		assertTrue("The first item should be 1", list.get(0) == 1);
		assertTrue("The second item should be 3", list.get(1) == 3);
	}
	
	@Test
	public void testDeleteEndOfThree() {
		DList<Integer> list = new DList<Integer>();
		list.append(1);
		list.append(2);
		list.append(3);
		list.delete(2);
		assertTrue("The list should be of getLength 2", list.getLength() == 2);
		assertTrue("The first item should be 1", list.get(0) == 1);
		assertTrue("Tthe second item should be 2", list.get(1) == 2);
	}
	
	@Test
	public void testDeleteAllOfThreeOrder0() {
		DList<Integer> list = new DList<Integer>();
		list.append(1);
		list.append(2);
		list.append(3);
		list.delete(0);
		list.delete(0);
		list.delete(0);
		assertTrue("The getLength should be of getLength 0", list.getLength() == 0);
		assertNull("Front should be null", list.getFront());
		assertNull("Back should be null", list.getBack());
	}
	
	@Test
	public void testDeleteAllOfThreeOrder1() {
		DList<Integer> list = new DList<Integer>();
		list.append(1);
		list.append(2);
		list.append(3);
		list.delete(2);
		list.delete(1);
		list.delete(0);
		assertTrue("The getLength should be of getLength 0", list.getLength() == 0);
		assertNull("Front should be null", list.getFront());
		assertNull("Back should be null", list.getBack());
	}

}
